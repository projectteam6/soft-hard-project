import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Checklist from "@/types/checklist";
import checklistService from "@/services/checklist";
import { useSelectStore } from "./selectMenu";
import { usemanagePopupStore } from "@/stores/popupManagement";

export const useChecklistStore = defineStore("checklist", () => {
  const popupManagement = usemanagePopupStore();
  const dialog = ref(true);
  const addChecklistUnits = ref(["ฟอง", "กิโลกรัม", "ชิ้น", "ลิตร"]);
  const checklists = ref<Checklist[]>([]);
  const checklistSelected = ref<Checklist>();
  const checklistsByUnit = ref<Checklist[]>([]);
  const editedChecklist = ref<Checklist>({
    name: "",
    amount: 0,
    minimum: 0,
    unit: "",
  });
  watch(checklists, () => {
    getChecklists();
  });
  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedChecklist.value = {
        name: "",
        amount: 0,
        minimum: 0,
        unit: "",
      };
    }
  });

  //For first time when logged id.
  async function getChecklists() {
    try {
      const res = await checklistService.getChecklists();
      checklists.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function saveChecklist() {
    try {
      //use when edit checklist
      if (editedChecklist.value.id) {
        const res = await checklistService.updateChecklist(
          editedChecklist.value.id,
          editedChecklist.value
        );
      }
      //use when add new checklist
      else {
        const res = await checklistService.saveChecklist(editedChecklist.value);
      }
      dialog.value = false;
      await getChecklists();
    } catch (e) {
      console.log(e);
    }
  }

  function editChecklist(checklist: Checklist) {
    editedChecklist.value = JSON.parse(JSON.stringify(checklist));
    dialog.value = true;
  }

  async function deleteChecklist(id: number) {
    try {
      const res = await checklistService.daleteChecklist(id);
      await getChecklists();
    } catch (e) {
      console.log(e);
    }
  }

  const getChecklist = async (id: number) => {
    return await checklistService.getChecklistByID(id);
  };

  return {
    addChecklistUnits,
    checklists,
    getChecklists,
    getChecklist,
    dialog,
    checklistsByUnit,
    editedChecklist,
    saveChecklist,
    editChecklist,
    deleteChecklist,
    checklistSelected,
  };
});
