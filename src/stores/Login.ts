import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./users";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import auth from "@/services/auth";
import router from "@/router";
import userService from "@/services/user";
export const useLoginStore = defineStore("login", () => {
  const dialog = ref(false);
  const loadingStore = useLoadingStore();
  const userStore = useUserStore();
  const loginName = ref("");
  const img = ref("");
  const position = ref("");
  const positionForMenu = ref("");
  const messageStore = useMessageStore();
  const isLogin = computed(() => {
    //loginName is not empty ทำไมต้องใช้ computed
    // return loginName.value !== "";
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  });
  const currentChef = ref("");
  const currentEmp = ref("");
  const currentManager = ref("");
  const login = async (userName: string, password: string): Promise<void> => {
    // const user = userStore.login(userName, password);
    // loginName.value = userName;
    // if (user.id !== 0) {
    //   loginName.value = userName;
    //   img.value = user.img;
    //   position.value = user.position;
    //   localStorage.setItem("loginName", userName);
    // } else {
    //   messageStore.showMessage("Login หรือ Password ไม่ถูกต้อง!!");
    // }
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(userName, password);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      const userID: number = res.data.user.id;
      const emp = await userService.getUserByID(userID);
      console.log(emp.data.employee.firstname);
      console.log(res.data.user.position);
      localStorage.setItem("positionForMenu", res.data.user.position);
      if (res.data.user.position === "พ่อครัว") {
        currentChef.value = emp.data.employee.firstname;
        router.push("/cook");
        console.log(res.data.user.position);
        localStorage.setItem("currentChef", emp.data.employee.firstname);
      } else if (res.data.user.position === "พนักงาน") {
        currentEmp.value = emp.data.employee.firstname;
        localStorage.setItem("currentEmp", emp.data.employee.firstname);
        router.push("/table");
      } else if (res.data.user.position === "ผู้บริหาร") {
        currentManager.value = emp.data.employee.firstname;
        router.push("/report");
        console.log(res.data.user.position);
        localStorage.setItem("currentManager", emp.data.employee.firstname);
      }
    } catch (e) {
      console.log(e);
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
  };

  const logout = () => {
    // loginName.value = "";
    // localStorage.removeItem("loginName");
    // dialog.value = false;
    // router.push({ path: "/login", replace: true });

    localStorage.removeItem("user");
    localStorage.removeItem("token");
    dialog.value = false;
    localStorage.removeItem("currentEmp");
    localStorage.removeItem("currentChef");
    router.replace("/login");
  };

  const loadData = () => {
    // loginName.value = localStorage.getItem("loginName") || "";
  };

  return {
    currentEmp,
    currentChef,
    isLogin,
    login,
    logout,
    loadData,
    loginName,
    img,
    position,
    dialog,
    positionForMenu,
  };
});
