import { ref, watch } from "vue";
import { defineStore } from "pinia";
import checkInOutService from "@/services/checkInOut";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import userService from "@/services/user";
import type CheckInOut from "@/types/checkInOut";
import auth from "@/services/auth";

export const useCheckInOutStore = defineStore("checkInOut", () => {
  const checkInOuts = ref<CheckInOut[]>([
    {
      hour: 0,
      status: "",
    },
  ]);

  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);

  const editedCheckInOut = ref<{ username: string; password: string }>({
    username: "",
    password: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCheckInOut.value = {
        password: "",
        username: "",
      };
    }
  });

  //For first time when logged id.
  async function getCheckInOuts() {
    try {
      const res = await checkInOutService.getCheckInOut();
      checkInOuts.value = res.data;
      console.log(res.data);
    } catch (e) {
      console.log(e);
    }
  }
  const getCheckInOut = async (id: number) => {
    return await checkInOutService.getCheckInOutByID(id);
  };

  async function saveCheckInOut() {
    let isValid = true;
    try {
      const res = await userService.userLogin(
        editedCheckInOut.value.username,
        editedCheckInOut.value.password
      );

      if (res.data !== false) {
        console.log(res.data);
        for (let i = 0; i < checkInOuts.value.length; i++) {
          const element = checkInOuts.value[i];
          if (element.employee?.id === res.data.employee?.id) {
            // ใช้ optional chaining operator
            console.log(
              "true " + element.employee?.id + " " + res.data.employee?.id // ใช้ optional chaining operator
            );
            isValid = false;
            messageStore.showError("มีชื่ออยู่ในระบบแล้ว");
            console.log("ชื่อซ้ำ");
            break;
          }
        }
        if (isValid) {
          const checkInOut: CheckInOut = {
            employee: res.data.employee,
            hour: 0,
            status: "ยังไม่ชำระ",
          };
          await checkInOutService.saveCheckInOut(checkInOut);
        }
      } else {
        isValid = false;
        messageStore.showError("Username หรือ Password ผิด");
        console.log("รหัสผิด");
      }
    } catch (e) {
      console.log(e);
      isValid = false;
      console.log("ไม่รู้");
    }
    return isValid;
  }

  async function deleteCheckInOut(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await checkInOutService.daleteCheckInOut(id);
      await getCheckInOuts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ CheckIn-Out ได้");
    }
    loadingStore.isLoading = false;
  }

  function editCheckInOutkub(checkInOut: CheckInOut) {
    editedCheckInOut.value = JSON.parse(JSON.stringify(checkInOut));
    dialog.value = true;
  }

  async function checkOut(checkInOut: CheckInOut) {
    console.log(checkInOut.timestart);
    try {
      const diff =
        new Date().valueOf() - new Date(checkInOut.timestart!).getTime();
      const diffInHours = diff / 1000 / 60 / 60; // Convert milliseconds to hours
      const newCheckInOut: CheckInOut = {
        timeend: new Date(),
        hour: diffInHours,
      };
      await checkInOutService.updateCheckInOut(checkInOut.id!, newCheckInOut);
      console.log("updated CheckInOut");
      console.log("diff: " + new Date(checkInOut.timestart!).getTime());
    } catch (e) {
      console.log(e);
    }
  }

  return {
    dialog,
    checkOut,
    getCheckInOuts,
    checkInOuts,
    getCheckInOut,
    editedCheckInOut,
    deleteCheckInOut,
    saveCheckInOut,
    editCheckInOutkub,
  };
});
