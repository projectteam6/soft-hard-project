import type Food from "./Food";
import type FoodQueue from "./foodQueue";
import type Order from "./order";
import type Table from "./Table";

export default interface OrderItem {
  id?: number;
  name: string;
  note?: string;
  table?: Table;
  qty: number;
  price: number;
  status?: string;
  order?: Order;
  food?: Food;
  foodQueue?: FoodQueue[];
  // id: number;
  // food: Food; //use food id as type Food
  // name: string;
  // note: string;
  // table: Table;
  // qty: number;
  // totol: number;
  // price: number;
  // status: string;
  // order: Order;
}
