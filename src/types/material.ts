export default interface Material {
  id?: number;
  name?: string;
  amount?: number;
  minimum?: number;
  unit?: string;
}
