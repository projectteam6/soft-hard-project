import type Employee from "./employee";

export default interface Salary {
  id?: number;
  salary: number;
  date?: Date;
  employee?: Employee;
}
