import type checklists from "@/types/checklist";
import http from "./axios";
function getChecklists() {
  return http.get("/checklists");
}

function getChecklistByID(id: number) {
  return http.get(`/checklists/${id}`);
}

function getUnit(unit: string) {
  return http.get(`/checklists/type/${unit}`);
}

function saveChecklist(Checklist: checklists) {
  return http.post("/checklists", Checklist);
}

function updateChecklist(id: number, Checklist: checklists) {
  return http.patch(`/checklists/${id}`, Checklist);
}

function daleteChecklist(id: number) {
  return http.delete(`/checklists/${id}`);
}
export default {
  getChecklists,
  getChecklistByID,
  saveChecklist,
  updateChecklist,
  daleteChecklist,
  getUnit,
};
