import type Salary from "@/types/salary";
import http from "./axios";
function getSalary() {
  return http.get("/salarys/findAllSalary");
}

function getSalaryByID(id: number) {
  return http.get(`/salarys/${id}`);
}
function getSalaryByName(name: string) {
  return http.get(`/salarys/${name}/findSalaryByName`);
}

function saveSalary(salary: Salary) {
  return http.post("/salarys", salary);
}

function updateSalary(id: number, salary: Salary) {
  return http.patch(`/salarys/${id}`, salary);
}

function daleteSalary(id: number) {
  return http.delete(`/salarys/${id}`);
}
export default {
  getSalary,
  getSalaryByID,
  saveSalary,
  updateSalary,
  daleteSalary,
  getSalaryByName,
};
